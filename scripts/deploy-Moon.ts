// @ts-ignore
import hre, { ethers } from "hardhat";
import { Contract } from "ethers";

const fs = require("fs");
const path = require("path");
const util = require("util");
const writeFile = util.promisify(fs.writeFile);

async function main() {


    const Moon = await ethers.getContractFactory("Moon");
    const MoonClubInst = await Moon.deploy();

    const deployment = {
        MoonClubInst: MoonClubInst.address.toLowerCase(),
    }
    console.log("MoonClubInst deployment:\n", JSON.stringify(deployment));
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
